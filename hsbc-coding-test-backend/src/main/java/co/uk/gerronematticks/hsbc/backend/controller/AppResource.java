package co.uk.gerronematticks.hsbc.backend.controller;

import co.uk.gerronematticks.hsbc.backend.model.Cities;
import co.uk.gerronematticks.hsbc.backend.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
public class AppResource {

    private final CityService cityService;

    @Autowired
    public AppResource(CityService cityService) {
        this.cityService = cityService;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/city", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Mono<Cities> getCities(@RequestParam(required = false) String name) {
        return cityService.getCities(name);
    }

}
