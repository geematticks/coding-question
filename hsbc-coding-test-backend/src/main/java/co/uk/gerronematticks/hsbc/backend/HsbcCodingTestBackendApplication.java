package co.uk.gerronematticks.hsbc.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("co.uk.gerronematticks.hsbc.backend")
public class HsbcCodingTestBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(HsbcCodingTestBackendApplication.class, args);
	}

}
