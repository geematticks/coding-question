package co.uk.gerronematticks.hsbc.backend.openweather;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class OpenWeatherMapClient {

    private final WebClient client;

    public OpenWeatherMapClient(@Value("${backend.weather.client}") String weatherClient) {
        this.client = WebClient.create(weatherClient);
    }

    public Mono<Response> getWeatherData() {
        return this.client.get().uri("/data/2.5/box/city?bbox=12,32,15,37,10&appid=b6907d289e10d714a6e88b30761fae22")
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(Response.class));
    }


}
