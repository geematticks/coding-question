package co.uk.gerronematticks.hsbc.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Cities {
    private final List<City> cities;
    private final Integer noOfCities;
}
