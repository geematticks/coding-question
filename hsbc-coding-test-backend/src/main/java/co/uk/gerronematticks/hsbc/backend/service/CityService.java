package co.uk.gerronematticks.hsbc.backend.service;

import co.uk.gerronematticks.hsbc.backend.model.Cities;
import co.uk.gerronematticks.hsbc.backend.model.City;
import co.uk.gerronematticks.hsbc.backend.openweather.OpenWeatherMapClient;
import co.uk.gerronematticks.hsbc.backend.openweather.Response;
import io.netty.util.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CityService {

    private final OpenWeatherMapClient openWeatherMapClient;

    @Autowired
    public CityService(OpenWeatherMapClient openWeatherMapClient) {
        this.openWeatherMapClient = openWeatherMapClient;
    }

    public Mono<Cities> getCities(@Nullable String cityNameFilter) {
        Mono<Response> weatherData = this.openWeatherMapClient.getWeatherData();

        return weatherData.map(it -> mapResponse(cityNameFilter, it));
    }

    private Cities mapResponse(@Nullable String cityNameFilter, Response it) {
        List<City> collect = it.getList().stream()
                .filter(cityResponse -> filterCityName(cityNameFilter, cityResponse))
                .map(cityResponse -> new City(cityResponse.getName()))
                .collect(Collectors.toList());

        return new Cities(collect, collect.size());
    }

    private boolean filterCityName(@Nullable String cityNameFilter, Response.CityResponse cityResponse) {
        if (StringUtil.isNullOrEmpty(cityNameFilter)) {
            return true;
        }

        return cityResponse.getName().toLowerCase().startsWith(cityNameFilter.toLowerCase());
    }


}
