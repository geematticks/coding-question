package co.uk.gerronematticks.hsbc.backend.model;

import lombok.Getter;

@Getter
public class City {

    private final String name;

    public City(String name) {
        this.name = name;
    }
}
