package co.uk.gerronematticks.hsbc.backend.openweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
public class Response {

    private final String cod;
    private final List<CityResponse> list;

    public Response(@JsonProperty("cod") String cod,
                    @JsonProperty("list") List<CityResponse> list) {
        this.cod = cod;
        this.list = list;
    }

    @Getter
    public static class CityResponse {

        private final String name;

        public CityResponse(@JsonProperty("name") String name) {
            this.name = name;
        }
    }

}
