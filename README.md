# HSBC Coding Question (Gerrone Matticks)

Contains
1. hsbc-coding-test-backend - provides REST API for city endpoint written in Java w/ Spring boot
2. hsbc-coding-test-frontend - Front end UI written in ReactJS
3. ngnix-proxy - Proxy server which forwards requests for the front end and back end

## How to Run

### Option 1. Requires Docker
Simply build & run via.
```
docker-compose up -d --build
```
And shut down with the following command
```
docker-compose down
```

### Option 2. Without Docker
1. First run the Back end
```
cd hsbc-coding-test-backend
mvn spring-boot:run
```
2. Run front end in seperate terminal window
```
cd hsbc-coding-test-frontend
yarn start
```

The application will be available at http://localhost:3000/