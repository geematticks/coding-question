export interface CityResponse {
    cities: City[];
    noOfCities: number
}

export interface City {
    name: string;
}