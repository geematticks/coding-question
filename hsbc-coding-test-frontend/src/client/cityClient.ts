import Axios, {AxiosInstance} from "axios";
import {CityResponse} from "./response";

export class CityClient {


    private readonly BACK_END_API_URL: string;
    private readonly axoisClient: AxiosInstance


    constructor(BACK_END_API_URL: string = process.env.REACT_APP_BACK_END_API || "") {
        this.BACK_END_API_URL = BACK_END_API_URL;
        this.axoisClient = Axios.create();
    }

    async getCities(cityName: string): Promise<CityResponse> {
        const axiosResponse = await this.axoisClient.request({
            method: "GET",
            baseURL: this.BACK_END_API_URL,
            url: '/city',
            params: {
                name: cityName
            }
        });

        return axiosResponse.data

    }

}