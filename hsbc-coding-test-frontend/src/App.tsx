import React, {ChangeEvent, useState} from 'react';
import './App.css';
import {CityClient} from "./client/cityClient";
import {CityResponse} from "./client/response";

const App: React.FC = () => {
    const cityClient = new CityClient();
    const [noOfCities, setNoOfCities] = useState<number | undefined>(undefined)

    const onCityChange = (e: ChangeEvent<HTMLInputElement>) => {
        cityClient.getCities(e.target.value).then((r: CityResponse) => setNoOfCities(r.noOfCities));
    };

    return (
        <div className="App">
            <header className="App-header">

                <input type="text" onChange={onCityChange} placeholder="Enter City Name"/>

                <div>
                    <span>No Of Cities Matching: </span>
                    <span>{noOfCities}</span>
                </div>
            </header>te
        </div>
    );
};

export default App;
